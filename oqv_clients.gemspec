$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "oqv_clients/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "oqv_clients"
  s.version     = OqvClients::VERSION
  s.authors     = ["Victor Alencar"]
  s.email       = ["victor.a.alencar@gmail.com"]
  s.homepage    = "http://bitbucket.com/oqv/oqv_clients"
  s.summary     = "OqvClients its a gem to access client data from Rails projects."
  s.description = "Gem to access clients data api"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", ">= 4.1.0"
  s.add_dependency "rest-client", "~> 1.8.0"


  s.add_development_dependency "mysql2"
  s.add_development_dependency "byebug"
end
