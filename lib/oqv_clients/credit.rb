module OqvClients
  class Credit < Base

    def all(client_id=nil)
      get_api(build_credit_url(client_id), access_token)
    end
    
    def reserve(client_id=nil, order_id, value)
      post_api("#{build_credit_url(client_id)}/#{order_id}/reserve", { credit: { value: value } }, access_token)
    end

    def debit(client_id=nil, order_id)
      post_api("#{build_credit_url(client_id)}/#{order_id}/debit", {}, access_token)
    end

    def cancel(client_id=nil, order_id)
      post_api("#{build_credit_url(client_id)}/#{order_id}/cancel", {}, access_token)
    end

    def insert(client_id=nil, order_id, value, voucher)
      post_api("#{build_credit_url(client_id)}/#{order_id}/insert", { credit: { value: value, voucher: voucher }}, access_token)
    end

    def build_credit_url(client_id=nil)
      read_id = client_id || id
      "#{client_url}/#{read_id}/credits"
    end

  end
end
