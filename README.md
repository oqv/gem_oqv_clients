# README #

Documentação da GEM para acesso a API de clientes pelos projetos em Rails.

### Setup ###

A gem pode acessar uma conta específica pré-configurada, um Admin por exemplo.

Assim como pode acessar um usuário específico informando seu e-mail e senha no momento de gerar o token.

Então, fica opcional configurar um usuário Admin default. Abaixo as ENVs:

- **CLIENT_EMAIL_API** *opcional
- **CLIENT_PASSWORD_API** *opcional
- **CLIENT_API_URL**

### Utilização ###

-- **Token**

A primeira chamada que deve ser feita ao instanciar é gerar o token. Ex:

Utilizando cliente pré-configurado pela ENV:

```
#!ruby
client = OqvClients::Client.new
client.generate_access_token!
```

Ou informando um:

```
#!ruby
client = OqvClients::Client.new
client.generate_access_token!(email, password)
```

-- **Outras funções do Cliente**

Após gerar o token, ele fica gravado na instância da classe no atributo access_token.

A partir daí você pode chamar as funções específicas como:

```
#!ruby
client.get # Informa os dados do usuário
client.get(1) # Informa os dados do usuário específicos (se for usuário admin)
client.search(params) # Busca clientes - Params: { q: {name_cont: '123'} } - Ransack
client.addresses # Informa os endereços
client.create(client) # Cria o cliente, o parametro é um hash assim: { client: { name: 'Chora me liga' } }
client.update(client) # Atualiza o cliente instanciado, o parametro é um hash assim: { client: { name: 'Chora me liga' } }
client.update(client, client_id) # Atualiza o cliente específico, o objeto é um hash assim: { client: { name: 'Chora me liga' } } (se for usuário admin)
client.is_token_valid? # Verifica se token permanece válido
```