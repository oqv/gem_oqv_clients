require "rest-client"

require "oqv_clients/version"
require "oqv_clients/configuration"
require "oqv_clients/base"
require "oqv_clients/client"
require "oqv_clients/credit"

module OqvClients

  class << self
    attr_writer :configuration
  end

  def self.configuration
    @configuration ||= Configuration.new
  end

  def self.configure
    yield(configuration)
  end

end
